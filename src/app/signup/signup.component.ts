import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styles: []
})
export class SignupComponent implements OnInit {

  constructor(public authservice:AuthService,
    public router:Router) { }

  email: string;
  password: string;
  

  onSubmit(){
    this.authservice.Signup(this.email, this.password);
    this.router.navigate(['/books']);
  }
  ngOnInit() {

  }

}

