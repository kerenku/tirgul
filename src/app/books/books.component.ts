import { Component, OnInit } from '@angular/core';
import {BooksService} from './../books.service'
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  panelOpenState = false;
  //books: object[] =  [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}]
  //books: object[];
  books$:Observable<any[]>;
  userId:string;

  deleteBook(id){
    this.bookservice.deleteBook(id,this.userId)
    console.log(id);
  }
  
  constructor(private bookservice:BooksService,
              public authService:AuthService) { }
              ngOnInit() { 
                console.log("NgOnInit started")  
                this.authService.getUser().subscribe(
                  user => {
                    this.userId = user.uid;
                    this.books$ = this.bookservice.getBooks(this.userId); 
                  }
                )
              }
            }
            