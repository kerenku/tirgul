import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AuthService } from './auth.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BooksService {


  books:any = [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}, {title:'A Song Of Ice And Fire', author:'George R R Martin'}]
 
  userCollection: AngularFirestoreCollection = this.db.collection('users');
  bookCollection: AngularFirestoreCollection; 
 
    
  getBooks(userId): Observable<any[]> {
    //const ref = this.db.collection('books');
    //return ref.valueChanges({idField: 'id'});
    this.bookCollection = this.db.collection(`users/${userId}/books`);
    console.log('Books collection created');
    return this.bookCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        data.id = a.payload.doc.id;
        return { ...data };
      }))
    );    
  } 


    addBook(userId:string, title:string, author:string){
      const book = {title:title, author:author}
      //this.db.collection('books').add(book);
      this.userCollection.doc(userId).collection('books').add(book);
    }
    
    deleteBook(id:string, userId:string){
      this.db.doc(`users/${userId}/books/${id}`).delete();
    }
  
  
  updateBook(userId:string, id:string, title:string, author:string){
    this.db.doc(`users/${userId}/books/${id}`).update({
      title:title,
      author:author
    })
  }


  // getBook(id:string):Observable<any>{
  //   return this.db.doc(`books/${id}`).get()
  // }

  getBook(id:string, userId:string):Observable<any>{
    return this.db.doc(`users/${userId}/books/${id}`).get()
  }
  
  
  
constructor(private db:AngularFirestore,public authservice:AuthService) {}


}